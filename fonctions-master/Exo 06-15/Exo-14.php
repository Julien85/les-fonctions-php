<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
</head>
    <body>
            
        <?php

             // $x et $y stockent un chiffre entre 1 et 100
             // $z stock un opérateur arithmétique
             // Peut importe les chiffres et l'opérateur, affichez le bon résultat
             // Exemple : 27 x 77 = 2079
            
        ?>
            
        <!-- écrire le code après ce commentaire -->
            
            	<?php
                    
                    $x = rand(1,100);
                    $y = rand(1,100);
                    $w = ['+', '-', '*', '/'];
                    $z = $w[rand(0,3)];
                
                    function calcul($a, $b, $c){
                        if($c == '+'){
                            echo $a .' '. $c.' '. $b .' = '.($a+$b);
                        }elseif($c == '-'){
                            echo $a.' '. $c.' '. $b .' = '.($a-$b);
                        }elseif($c == '*'){
                            echo $a .' '. $c.' '. $b .' = '.($a*$b);
                        }else{
                            echo $a .' '. $c.' '. $b .' = '.($a/$b);
                        }
                    }
                    calcul($x, $y, $z);
                ?>
            
        <!-- écrire le code avant ce commentaire -->
        
    </body>
</html>