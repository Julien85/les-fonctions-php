<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
</head>
    <body>
            
        <?php
    
             // Faites une fonction qui retire toutes les voyelles dans un texte 
             //et qui calcule la différence entre le nombre de caractères au départ,
             // et le nombre de caractères une fois les voyelles retirées

            $texte = 'Je ne sais pas combien il y a de e dans ce texte mais il commence à y en avoir beaucoup';
        ?>
            
        <!-- écrire le code après ce commentaire -->
            
            
            	<?php

                    $vowels = array("a", "e", "i", "o", "u", "y");


                    function supp($i, $j){
                        $remplace = str_replace($j, "", $i);
                        return $remplace;
                    }

                    echo $texte .'<br><br>';

                    echo supp($texte, $vowels).'<br><br>';

                    $totalletters = iconv_strlen($texte);

                    echo "Le nombre de caractères total du texte est de : " . $totalletters .'<br><br>';

                    $letters = iconv_strlen(supp($texte, $vowels));

                    echo "Le nombre de caractères du texte sans voyelles est de : ". $letters .'<br><br>';

                    $texteSansVoyelles = $totalletters-iconv_strlen(supp($texte, $vowels));

                    echo "Le nombre de voyelles est de : " . $texteSansVoyelles .'<br><br>';


                ?>
            
        <!-- écrire le code avant ce commentaire -->
        
    </body>
</html>