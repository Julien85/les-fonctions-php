<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
</head>
    <body>
            
        <?php
    
             // Faites une fonction qui retourne le nombre de lettres dans la chaine envoyée
             // et combien de fois il y a la lettre recherchée.
            
             $texte = 'Je ne sais pas combien il y a de e dans ce texte mais il commence à y en avoir beaucoup';
             $search = 'e';
        ?>
            
        <!-- écrire le code après ce commentaire -->
            
            
                <?php

                function nbDeLettres($nb){
                    $totalDeLettres = iconv_strlen($nb);
                    return $totalDeLettres;
                }
                echo "Il y a ". nbDeLettres($texte) ." lettres dans la variable texte.<br>";

                function cherche($letters, $i){
                    $caractere = substr_count($letters, $i);
                    return $caractere;
                }
                echo "Il y a ". cherche($texte, $search) ." lettres " .$search. " dans la variable texte.<br>";
   
                ?>
            
        <!-- écrire le code avant ce commentaire -->
        
    </body>
</html>