<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
</head>
    <body>
            
        <?php
    
             // Faites un formulaire afin de récuperer la largeur, la longeur et
             // le nombre de niveaux d'un maison
             // Faites une fonction afin de calculer la surface de la maison
             // A l'exterieur de la fonction, multiplier cette valeur par 10 afin de connaitre 
             //la taille d'un futur immeuble
            
        ?>
            
        <!-- écrire le code après ce commentaire -->
            
        <?php
        
        $recuplargeur = isset($_GET["largeur"]) ? intval($_GET["largeur"]) : 0;
        $recuplongueur = isset($_GET["longueur"]) ? intval($_GET["longueur"]) : 0;
        $recupnb = isset($_GET["nb"]) ? intval($_GET["nb"]) : 0;


        ?>  

        <form action="Exercice08.php" method="get">
            <p>Largeur : <input type="text" name="largeur" value=<?php if(!empty($_GET["largeur"])){echo $_GET["largeur"];}?>></p>
            <p>Longueur : <input type="text" name="longueur" value=<?php if(!empty($_GET["longueur"])){echo $_GET["longueur"];}?> ></p>
            <p>Nb de niveaux : <input type="text" name="nb" value=<?php if(!empty($_GET["nb"])){echo $_GET["nb"];}?> ></p>

            <p><input type="submit" value="OK"></p>
        </form>

        <?php

        

        function totalsurface($largeur, $longueur, $etage){
            $surface = $largeur*$longueur*$etage;
            return $surface;
        }

        function immeuble($hlm){
            $tour = $hlm*10;
            return $tour;
        }

        echo "<h3>La maison fait : ".totalsurface($recuplargeur, $recuplongueur, $recupnb)." m2.</h3>";

        echo "<h3>Le futur immeuble fera : ".immeuble(totalsurface($recuplargeur, $recuplongueur, $recupnb))." m2.</h3>";
        ?>
          
        <!-- écrire le code avant ce commentaire -->
        
    </body>
</html>