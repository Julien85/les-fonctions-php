<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
</head>
    <body>
            
        <?php
    
            // Créer une fonction qui complète automatiquement le genre des hommes
            // Lorsque vous lui envoyer un prenom d'homme elle renvoie:
            // Bonjour Jean, après vérification vous êtes un homme

            // Si vous lui envoyez un prenom de femme, préciser le genre 'femme' dans les arguments
            // Bonjour Sonia, après vérification vous êtes une femme

        ?>
            
        <!-- écrire le code après ce commentaire -->
            
                <?php
                
                function statut($prenom, $sex='homme'){
                    echo 'Salut '.$prenom.$sex.'<br>';
                }
                statut(' Paul ');
                statut('Sonia ', ' femme ')

                ?>
            
        <!-- écrire le code avant ce commentaire -->
        
    </body>
</html>